#include "snake.h"

int main()
{
    time(&initial_time); // measure time at start of program

    // greeting and game instructions
#ifndef DISABLE_START_MSG
    print_start_msg();
#endif

    // initialization
    curses_init();
    game_init();

    // main loop
    time_t old_time = initial_time;
    do {
        if (paused) {
            handle_game_pause();
            continue;
        }

        update_timers();
        get_input();

        if (curr_time - old_time >= 1) {
            spawn_food(); // run every second, not every frame
            old_time = curr_time;
        }

        move_snake();
        check_loss();
        check_food_collision();
        update_frame();
    } while (input != 'q' && !game_over);

    // end
    return end_game();
}

void handle_game_pause()
{
    // centered message
    static const char pause_msg[] = "PAUSED";
    mvprintw(screen_height / 2, screen_width / 2 - strlen(pause_msg) / 2, "PAUSED");
    refresh();
    get_input(); // in case the user wants to unpause
}

void print_start_msg()
{
    printf("Welcome to Cursed Snake!\n");
    printf("\nHOW TO PLAY:\n");
    printf("\t- move with w,a,s,d;\n");
    printf("\t- press p to pause and unpause the game;\n");
    printf("\t- press q at any time to quit.\n");
    printf("\nPress ENTER to start the game...\n");
    GET_CHAR(input, junk_ptr); // gets rid of the whole line from stdin
}

void update_timers()
{
    time(&curr_time); // get current time

    // update elapsed minutes and seconds
    time_played.seconds = curr_time - initial_time;
    if (time_played.seconds >= SECONDS_PER_MIN) {
        time_played.minutes = time_played.seconds / SECONDS_PER_MIN;
        time_played.seconds %= SECONDS_PER_MIN;
    }

    feed_time_s = curr_time - feed_time;  // seconds until food respawns
}

void curses_init()
{
    win = initscr();
    timeout(READ_TIMEOUT); // timeout for getch function
    cbreak(); // exit with ctrl+c
    noecho(); // no keyboard echo
    curs_set(0); // disable cursor
    getmaxyx(win, screen_height, screen_width); // get root window size
}

void game_init()
{
    // disable drawing for each snake segm except the head
    for (int i = 1; i < MAX_SNAKE_LENGTH; ++i) {
        snake_segment[i].x = -1;
        snake_segment[i].y = -1;
    }

    // set the head in the middle of the screen
    snake_segment[0].x = screen_width / 2;
    snake_segment[0].y = screen_height / 2;

    // initialize food timer
    feed_time = initial_time;

    // get seed based on current real-world time
    struct tm tm = *localtime(&initial_time);
    if (!(rand_seed = tm.tm_hour * tm.tm_min * tm.tm_sec))
        rand_seed = tm.tm_hour + tm.tm_min + tm.tm_sec;
}

int end_game()
{
    endwin(); // end ncurses session

    // standard end of game messages
    printf("GAME OVER!\n");
    printf("LENGTH: %d\n", length);
    printf("TIME: %lldm %llds\n", time_played.minutes, time_played.seconds);

#ifdef DISABLE_STATS_SAVE // skip saving stats
    return 0;
#endif

    printf("\nDo you want to save your stats? [y/N]: ");
 
    // get user input (y or n)
    GET_CHAR(input, junk_ptr);
    free(junk_ptr); // free after last use
    switch(tolower(input)) {
        // 'yes' option
        case 'y':
            if (save_stats()) {
                printf("Succesfully saved stats to " SAVE_FILE ".\n");
                return 0;
            }
            else {
                printf("Error saving stats.\n");
                return -1;
            }

        // 'no' option (whitespace counts as no)
        case '\n':
        case '\t':
        case ' ':
        case 'n':
            return 0;

        // error
        default:
            printf("Invalid option: %c\n", input);
            return -1;
    }
}

bool save_stats()
{
    // get name from user
    char name[32] = "";
    printf("Enter your name: ");
    if (scanf("%31s", name) == 0)
        return false;

    // open save file in append mode
    FILE *save = fopen(SAVE_FILE, "a+");
    if (save == NULL)
        return false;

    // get date and time struct
    time_t t = time(NULL);
    struct tm tm = *localtime(&t);

    // write to save file
    int num_written = fprintf(save, "date/time: %d-%02d-%02d %02d:%02d:%02d\n"
                             "name: %s\nlength: %d\ntime played: %lldm %llds\n\n",
                             tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday, 
                             tm.tm_hour, tm.tm_min, tm.tm_sec,
                             name, length, time_played.minutes, time_played.seconds);
    fclose(save);

    // error while writing to the save file
    if (num_written < 0)
        return false;

    return true;
}


void update_frame()
{
    // clear the sceeen
    clear();

    // real-time stats
#ifdef ENABLE_STATS_DISPLAY
    mvprintw(0, 0, "Time played: %lldm %llds", time_played.minutes, time_played.seconds);
    mvprintw(1, 0, "Length: %d", length);
    mvprintw(2, 0, "Food respawns in: %llds", FOOD_RESPAWN_TIME - feed_time_s);

    // direction key press
    mvprintw(3, 0, "Movement direction: ");
    switch (input) {
        case 'a':
            printw("%s", "left");
            break;
        case 'd':
            printw("%s", "right");
            break;
        case 'w':
            printw("%s", "up");
            break;
        case 's':
            printw("%s", "down");
            break;

    }
#endif

    // draw each snake segm in order
    for (int i = 0; i < length; ++i)
        if (snake_segment[i].x != -1 && snake_segment[i].y != -1)
            mvaddch(snake_segment[i].y, snake_segment[i].x, SNAKE_SEGMENT_SYMBOL);

    // draw food
    if (food_pos.x != -1 && food_pos.y != -1)
        mvaddch(food_pos.y, food_pos.x, FOOD_SYMBOL);

    // draw frame
    refresh();
}


void get_input()
{
    // get movement input from user
    input = getch();
    switch (input) {
        case 'a': // move left
            if (!velocity.x) {
                velocity.x = -1;
                velocity.y = 0;
            }
            break;
        case 'd': // move right
            if (!velocity.x) {
                velocity.x = 1;
                velocity.y = 0;
            }
            break;
        case 's': // move down
            if (!velocity.y) {
                velocity.x = 0;
                velocity.y = 1;
            }
            break;
        case 'w': // move up
            if (!velocity.y) {
                velocity.x = 0;
                velocity.y = -1;
            }
            break;
        case 'p': // toggle game pause
            paused = !paused;
            break;
    }

    // calculate next head position
    next_head_pos.x = snake_segment[0].x + velocity.x;
    next_head_pos.y = snake_segment[0].y + velocity.y;
}

void move_snake()
{
    // shift all segments
    for (int i = length - 1; i > 0; --i)
        snake_segment[i] = snake_segment[i - 1];

    // update head position
    snake_segment[0].x = next_head_pos.x;
    snake_segment[0].y = next_head_pos.y;
}

void check_loss()
{
    // check if the snake touched the screen edges
    if (!IS_BETWEEN(snake_segment[0].x, 0, screen_width - 1) ||
        !IS_BETWEEN(snake_segment[0].y, 0, screen_height - 1)) {
        game_over = true;
        return;
    }

    // check self collision only if the snake is moving
    if (velocity.x || velocity.y) {
        // check the next position before updating the movement
        for (int i = 1; i < length; ++i)
            if (next_head_pos.x == snake_segment[i].x &&
                next_head_pos.y == snake_segment[i].y) {
                game_over = true;
                return;
            }
    }
}

void spawn_food()
{
    // respawn if time is up or food was eaten
    if (feed_time_s % FOOD_RESPAWN_TIME == 0 || 
            (food_pos.x == -1 && food_pos.y == -1)) {
        food_pos.x = rand_r(&rand_seed) % screen_width;
        food_pos.y = rand_r(&rand_seed) % screen_height;

        // reset food timer
        feed_time = curr_time;
    }
}

void check_food_collision()
{
    // check if the head collided with the food
    if (snake_segment[0].x == food_pos.x && snake_segment[0].y == food_pos.y) {
        if (length < MAX_SNAKE_LENGTH)
            ++length;

        // reset food position
        food_pos.x = -1;
        food_pos.y = -1;
    }
}
