CC=gcc
CFLAGS=-Wall -g -Iinclude
LIB=-lncurses

BIN=snake
SRC=snake.c

all: $(BIN)

$(BIN): $(SRC)
	$(CC) $(CFLAGS) -o $@ $< $(LIB)

clean:
	rm -f $(BIN)

.PHONY: all clean
