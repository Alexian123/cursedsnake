#ifndef __SNAKE_H__
#define __SNAKE_H__


#include <ncurses.h>
#include <stdbool.h>
#include <stdlib.h>
#include <time.h>
#include <ctype.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

// game settings
#include "settings.h"


#define SECONDS_PER_MIN 60

// check if NUM is between 2 other values
#define IS_BETWEEN(NUM,LEFT,RIGHT) (NUM >= LEFT && NUM <= RIGHT)

/* get the first char of a line from stdin and discard the rest; 
 * remember to free JUNK_PTR */
#define GET_CHAR(C,JUNK_PTR) {\
    size_t size=0;\
    getline(&JUNK_PTR, &size, stdin);\
    C = JUNK_PTR[0];\
    JUNK_PTR = NULL;\
}


// 2D vector type
typedef struct _v2i
{
    int x;
    int y;
} Vec2i;


// game time type
typedef struct _game_time
{
    long long minutes;
    long long seconds;
} GameTime;


WINDOW* win; // root ncurses window
int screen_width, screen_height; // root window limits

int length = STARTING_SNAKE_LENGTH; // number of active snake segments
bool game_over = false;
bool paused = false;

char input; // store user input
char *junk_ptr = NULL; // junk address for flushing stdin buffer

Vec2i next_head_pos = {0, 0};
Vec2i velocity = {0, 0};
Vec2i snake_segment[MAX_SNAKE_LENGTH];
Vec2i food_pos = {-1, -1}; // -1 means it does not get drawn

time_t initial_time; // saves the time at the start of the program
time_t feed_time; // food respawn timer
time_t curr_time; // keeps track of total elapsed time

GameTime time_played = {0, 0}; // time since starting the game (m & s)

long long feed_time_s; //  time since last food respawn (seconds)

unsigned int rand_seed; // seed for rand_r


// init & end
void print_start_msg();
void curses_init();
void game_init();
int end_game(); // returns error code
bool save_stats(); // returns false if it encounters an error

// drawing to screen
void update_frame();

// game logic
void handle_game_pause();
void update_timers();
void get_input();
void move_snake();
void check_loss();
void spawn_food();
void check_food_collision();


#endif
