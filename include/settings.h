#ifndef __SNAKE_SETTINGS_H__
#define __SNAKE_SETTINGS_H__

// display
//#define DISABLE_START_MSG // start the game immediately
#define ENABLE_STATS_DISPLAY // time, length etc.
//#define DISABLE_STATS_SAVE // don't ask the user about saving

// ascii symbols
#define SNAKE_SEGMENT_SYMBOL '@'
#define FOOD_SYMBOL '&'

// snake properties
#define MAX_SNAKE_LENGTH 100
#define STARTING_SNAKE_LENGTH 2

// time
#define READ_TIMEOUT 100 // affects move speed
#define FOOD_RESPAWN_TIME 10 // respawns food every n seconds

// saving
#define SAVE_FILE "stats.txt" // file for saving stats

#endif
